﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfApp110421b
{

    // 1 -- ICommand
    public class MyCommand : ICommand
    {
        public static bool change_me = true;
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            // calculate if the command is enabled or not?
            // enabled or not ?
            return change_me;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show("button clicked!");
        }
    }

    // 2 -- RelayCommand
    public class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private Func<object, bool> m_canExecute;
        private Action<object> m_execute;

        public RelayCommand(Func<object, bool> canExecute, Action<object> execute)
        {
            m_canExecute = canExecute;
            m_execute = execute;

        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_execute(parameter);
        }
    }

    public class MainWindowViewModel
    {
        //public DelegateCommand MyDelegate

        public MyCommand MyCommand { get; set; }

        public RelayCommand RelayCommand1 { get; set; }

        // 3 -- save you to create the relay command class
        public DelegateCommand DelegateCommand1 { get; set; }

        public async void Foo()
        {
            //await Task.Run(() =>
            //{
            //    Thread.Sleep(500);

            //});
            //MyCommand.change_me = true;
        }

        public MainWindowViewModel()
        {
            MyCommand = new MyCommand();
            RelayCommand1 = new RelayCommand(o => true, o => MessageBox.Show("relay1"));
            DelegateCommand1 = new DelegateCommand(
                () => MessageBox.Show("delegate command"),
                () =>
                {
                    return DateTime.Now.Second % 2 == 0;
                });
            Task.Run(() =>
            {
                while (true)
                {
                    DelegateCommand1.RaiseCanExecuteChanged();
                    Thread.Sleep(500);
                }
            });
        }
    }
}
